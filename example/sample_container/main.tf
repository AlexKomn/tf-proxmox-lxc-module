#module "container_template" {
#  source = "./module/container_template"  
#}

module "lxc_container" {
  source = "./module/lxc_container_instance"
  lxc_id = 333
  #lxc_id = var.lxc_id + count.index
  #count = 3
  #lxc_hostname = "${var.hostname}-${count.index}"
}

resource "random_password" "ubuntu_container_password" {
  length           = 16
  override_special = "_%@"
  special          = true
}

resource "tls_private_key" "ubuntu_container_key" {
  algorithm = "RSA"
  rsa_bits  = 2048
}