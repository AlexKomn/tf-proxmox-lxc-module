output "ubuntu_container_private_key" {
  value = module.lxc_container.ubuntu_container_private_key
  sensitive = true
}

output "ubuntu_container_public_key" {
  value = module.lxc_container.ubuntu_container_public_key
  sensitive = true
}