# Authentication
variable "proxmox_api_url" {
  description = "Proxmox UI/API url in form of https://<domain_nameORip>:8006/api2/json"
  type        = string
}

variable "proxmox_api_token_id" {
  type      = string
  sensitive = true
}

variable "proxmox_api_token_secret" {
  type      = string
  sensitive = true
}

# Variables for modules
variable "lxc_id" {
  type    = number
  default = 333
}
variable "hostname" {
  type    = string
  default = "container"
}

variable "ip_address_mask" {
  type        = string
  description = "IPv4 address with mask. Example 192.168.0.1/24"
}

variable "gateway" {
  type        = string
  description = "IPv4 address of the gateway without mask. Example 192.168.0.1"

}