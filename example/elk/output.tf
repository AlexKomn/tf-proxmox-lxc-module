output "ubuntu_container_private_key" {
  value     = tls_private_key.ubuntu_container_key.private_key_openssh
  sensitive = true
}

output "password" {
  value     = random_password.ubuntu_container_password.result
  sensitive = true
}