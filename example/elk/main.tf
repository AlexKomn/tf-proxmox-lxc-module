#module "container_template" {
#  source = "./module/container_template"  
#}

module "lxc_container" {
  source                = "../../module/lxc_container_instance"
  lxc_id                = 9200
  lxc_hostname          = "elk"
  lxc_ipv4_address_mask = var.ip_address_mask
  lxc_gateway           = var.gateway
  lxc_password          = random_password.ubuntu_container_password.result
  lxc_public_key        = tls_private_key.ubuntu_container_key.public_key_openssh
  lxc_container_cores   = 4
  lxc_memory_mb         = 4096
  container_tags        = ["elk", "terraform"]
}

resource "random_password" "ubuntu_container_password" {
  length           = 16
  override_special = "_%@"
  special          = true
}

resource "tls_private_key" "ubuntu_container_key" {
  algorithm = "RSA"
  rsa_bits  = 2048
} 