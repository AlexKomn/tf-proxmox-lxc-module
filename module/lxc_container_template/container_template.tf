resource "proxmox_virtual_environment_file" "ubuntu_container_template" {
  content_type   = "vztmpl"
  datastore_id   = "local"
  node_name      = var.proxmox_node_name
  overwrite      = false
  timeout_upload = 1800

  source_file {
    path = "http://download.proxmox.com/images/system/ubuntu-20.04-standard_20.04-1_amd64.tar.gz"
  }

  lifecycle {
    ignore_changes = [source_file]
  }
}