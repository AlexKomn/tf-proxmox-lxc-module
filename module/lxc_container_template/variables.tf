### Proxmox Settings#
variable "proxmox_node_name" {
  type        = string
  description = "Which node in the Proxmox cluster to start the virtual machine on during creation."
  default     = "proxmox"
}
