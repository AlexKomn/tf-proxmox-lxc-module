### Proxmox Settings#
variable "proxmox_node_name" {
  type        = string
  description = "Which node in the Proxmox cluster to start the virtual machine on during creation."
  default     = "proxmox"
}
### VM Settings ###
variable "lxc_id" {
  type        = number
  description = "The ID of the LXC container in Proxmox"
  default     = null
}
variable "lxc_hostname" {
  type    = string
  default = "container"
}
variable "lxc_ipv4_address_mask" {
  type    = string
  default = "dhcp"
}
variable "lxc_gateway" {
  type    = string
  default = null
}
variable "lxc_container_cores" {
  type        = number
  description = "Cores allocated to LXC container"
  default     = 1
}
variable "lxc_memory_mb" {
  type        = number
  description = "Memory allocated to LXC container"
  default     = 1024
}
variable "lxc_swap_memory_mb" {
  type        = number
  description = "Swap memory allocated to LXC container"
  default     = 0
}
variable "volume_size_G" {
  type        = number
  description = "The size of the volume in gigabytes"
  default     = 10
}
variable "container_tags" {
  type        = list(string)
  description = "Descriptive tags for container. Use lowercase letters. Default value: [\"terraform\"]"
  default     = ["terraform", "default"]
}
variable "template_id" {
  type        = string
  description = "Tempalte ID used for container. Passed from container_template module. Example proxmox/local:vztmpl/ubuntu-20.04-standard_20.04-1_amd64.tar.gz"
  default     = "local:vztmpl/ubuntu-20.04-standard_20.04-1_amd64.tar.gz"
}
variable "lxc_password" {
  type        = string
  description = "Password for container's root user, by default ssh password authentication turned off, so first time would be used in Proxmox console"
  default     = "default"
}
variable "lxc_public_key" {
  type        = string
  description = "Public key copied to the server as authorized key"
  default     = null
}

##### VM Post Config #####
#variable "user_public_key" {
#  type        = string
#  description = "The public key for the os user."
#}
#
#variable "os_username" {
#  type        = string
#  description = "The os username to be created."
#  default     = "cloud"
#  nullable    = false
#}
#