### Container template
resource "proxmox_virtual_environment_container" "ubuntu_container" {
  description = "Managed by Terraform"
  tags        = var.container_tags

  node_name    = var.proxmox_node_name
  vm_id        = var.lxc_id
  unprivileged = true

  initialization {
    hostname = var.lxc_hostname

    ip_config {
      ipv4 {
        address = var.lxc_ipv4_address_mask
        gateway = var.lxc_gateway
      }
    }
    user_account {
      # If key is empty -- pass empty tuple
      keys = var.lxc_public_key != null ? [
        trimspace(var.lxc_public_key)
      ] : []
      password = var.lxc_password
    }
  }

  cpu {
    cores = var.lxc_container_cores
  }

  memory {
    dedicated = var.lxc_memory_mb
    swap      = var.lxc_swap_memory_mb
  }

  network_interface {
    name = "vmbr0"
  }

  operating_system {
    template_file_id = var.template_id
    type             = "ubuntu"
  }

  disk {
    datastore_id = "local-lvm"
    size         = var.volume_size_G
  }

  console {
    enabled   = true
    tty_count = 2
    type      = "tty"
  }
}



